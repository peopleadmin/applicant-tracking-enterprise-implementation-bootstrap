# Install Chocolatey (https://chocolatey.org/install)
Set-ExecutionPolicy Bypass -Scope Process -Force
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')

# Makes "Update-SessionEnvironment" available
# https://stackoverflow.com/questions/46758437/how-to-refresh-the-environment-of-a-powershell-session-after-a-chocolatey-instal
Import-Module "$Env:ChocolateyInstall\helpers\chocolateyProfile.psm1"

# Make sure "choco" is available
Update-SessionEnvironment

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Upgrading Chocolatey...'

# Upgrade Chocolatey
choco 'upgrade' '-y' 'chocolatey'

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Checking if Hyper-V is enabled...'

# https://stackoverflow.com/questions/37567596/how-do-you-check-to-see-if-hyper-v-is-enabled-using-powershell
$GetWindowsOptionalFeatureResult = Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All

# Check if Hyper-V is enabled
if ($GetWindowsOptionalFeatureResult.State -eq "Enabled") {
	Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Hyper-V is enabled. No restart is needed.'
}
else {
	Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Hyper-V is disabled. Enabling it and restarting your computer. This action will only need to be performed once. After your computer restarts, resume setup by running the setup script again.'

	# Enable Hyper-V for Docker
	Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All -NoRestart

	Restart-Computer
}

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Uninstalling old and potentially conflicting Chocolately packages...'

# Uninstall outdated/incorrect packages if present
choco 'uninstall' '-y' 'jdk7'
choco 'uninstall' '-y' 'jdk8'
choco 'uninstall' '-y' 'javaruntime'
choco 'uninstall' '-y' 'jre8'
choco 'uninstall' '-y' 'svn'
choco 'uninstall' '-y' 'sql-server-express'

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Reinstalling needed Chocolatey packages...'

# Install/reinstall needed programs from Chocolatey
choco 'uninstall' '-y' 'adoptopenjdk8'
choco 'uninstall' '-y' 'ant'
# Have to go together because "git" depends on "git.install"
choco 'uninstall' '-y' 'git' 'git.install'
choco 'uninstall' '-y' 'tortoisesvn'
choco 'uninstall' '-y' 'graphviz'
choco 'uninstall' '-y' 'docker-desktop'

choco 'install' '-y' '--version' '8.232.9' 'adoptopenjdk8'
# "-i" is "IgnoreDependencies"
# Do not want to install https://chocolatey.org/packages/jre8, which is a dependency of ant
choco 'install' '-y' '--version' '1.10.5' '-i' 'ant'
choco 'install' '-y' '--version' '2.24.1.2' 'git'
choco 'install' '-y' '--version' '1.13.1.28686' 'tortoisesvn'
choco 'install' '-y' '--version' '2.38.0.20190211' 'graphviz'
choco 'install' '-y' '--version' '2.1.0.5' 'docker-desktop'

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Fixing environment variables...'

# These system environment variables are set by Chocolatey when install "adoptopenjdk8" and "ant"
# JAVA_HOME and ANT_HOME might be set to something old. Make sure we're using the paths set by Chocolatey.
$SystemJavaHome = [System.Environment]::GetEnvironmentVariable('JAVA_HOME', 'Machine')
$SystemAntHome = [System.Environment]::GetEnvironmentVariable('ANT_HOME', 'Machine')

# https://stackoverflow.com/questions/29190444/invalid-syntax-with-setx-for-having-more-than-two-arguments-when-there-are-onl
# https://serverfault.com/questions/664383/environment-variable
$SystemJavaHomeFixed =
if ($SystemJavaHome -match '[^\\]\\$') {
	"$SystemJavaHome\"
}
else {
	$SystemJavaHome
}

$SystemAntHomeFixed =
if ($SystemAntHome -match '[^\\]\\$') {
	"$SystemAntHome\"
}
else {
	$SystemAntHome
}

setx 'JAVA_HOME' $SystemJavaHomeFixed
setx 'ANT_HOME' $SystemAntHomeFixed

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Removing bad AutoRun registry keys...'

reg 'delete' 'HKEY_CURRENT_USER\Software\Microsoft\Command Processor' '/v' 'AutoRun' '/f'
reg 'delete' 'HKEY_LOCAL_MACHINE\Software\Microsoft\Command Processor' '/v' 'AutoRun' '/f'

# Make sure "git" is in the "PATH" (and that "JAVA_HOME" and "ANT_HOME" are updated)
Update-SessionEnvironment

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Configuring Git...'

# Store credentials forever
git 'config' '--global' 'credential.helper' 'store'
# https://stackoverflow.com/questions/35433326/how-to-set-a-git-config-variable-to-the-empty-value-from-the-command-line
# Don't let system-wide settings confuse things
git 'config' '--system' 'credential.helper' `"`"
# Don't use OpenSSL GUI for password entry
git 'config' '--global' 'core.askPass' `"`"

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Preparing to download implementation environment...'

$GetLocationDriveName = (Get-Location).Drive.Name

$GetLocationDriveNameWithColonIfNeeded =
if ($GetLocationDriveName -match '^[a-zA-Z]+$') {
	"${GetLocationDriveName}:"
}
else {
	$GetLocationDriveName
}

$GitCloneDirectory = Join-Path $GetLocationDriveNameWithColonIfNeeded 'applicant-tracking-enterprise-implementation'

Remove-Item $GitCloneDirectory -Recurse -Force

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'You will now be asked for your Bitbucket username and password. You will only have to enter these credentials once.'

# Save some time/bandwidth by doing a shallow clone
git 'clone' '-b' 'master' '--depth' '1' 'https://bitbucket.org/peopleadmin/applicant-tracking-enterprise-implementation.git' $GitCloneDirectory

$BatFilePath = Join-Path (Join-Path (Resolve-Path '~') 'Desktop') 'applicant-tracking-enterprise-implementation.bat'

Remove-Item $BatFilePath -Force

# Command Prompt only works with double quotes
Add-Content $BatFilePath "cd ""$GitCloneDirectory"""
Add-Content $BatFilePath 'ant "-p"'
Add-Content $BatFilePath 'cmd "/k"'

Set-Location $GitCloneDirectory

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'At this point, Docker should be installed on your computer. Before you can proceed, you must add your main drive as a "Shared Drive" in Docker (see setup guide). Press the enter key when you have done this.'

Read-Host

Write-Host -ForegroundColor White -BackgroundColor DarkGreen 'Setting up an ATE database in Docker...'

ant 'create-local-db'
