# Applicant Tracking Enterprise (ATE) implementation environment setup guide

This repository contains instructions for setting up the ATE implementation (sometimes referred to as "impl") environment, and a PowerShell script that does most of the setup automatically.

**These instructions assume that you are using Windows, since as far as I know, everyone doing support/services/implementation work on ATE uses Windows. If you need to get the implementation environment running on macOS or Linux, contact the ATE engineering team (Slack channel `#ps_eng_hireenterprise`). Alternatively, contact me (Andrew Liebenow) directly: `@Andrew Liebenow`.**

## Prerequisites

-   As mentioned above, your computer must be running Windows.
-   Access to the SearchSoft Trac server (used to be via Forticlient, now is via Cato Client; in the future will not require a VPN).
-   A Bitbucket account with read access to this repository: [https://bitbucket.org/peopleadmin/applicant-tracking-enterprise-implementation/src/master/](https://bitbucket.org/peopleadmin/applicant-tracking-enterprise-implementation/src/master/). You should have received an invite email from Bitbucket with some sort of language about ATE. Click the link in the email and follow the instructions to get access to the repository. If you do not have an invite email from Bitbucket, contact me (Andrew Liebenow).
-   You will also need to know your Bitbucket username. You can find this by clicking on your profile picture circle in the bottom left of Bitbucket, and then clicking on "Bitbucket settings". Your username will be listed under the heading "Bitbucket profile settings". Note down your username, and do not close your browser or navigate to another page. You now need to generate a password for your Bitbucket account. Click on "App passwords". Click "Create app password", and check all of the checkboxes on the page (don't worry if some of them are disabled/grayed out and can't be checked). Click "Create" and note down the auto-generated password that is displayed.

## Required manual steps

1.  Ensure that you have a file named `ats-user.properties` in a directory named `.searchsoft` in your user directory. If you don't know exactly what that file path would be on your computer, run the following command in a PowerShell terminal:

        echo $(Join-Path (Resolve-Path '~') '.searchsoft' 'ats-user.properties')

    The appropriate path will be displayed after you hit enter. It should be something like: `C:\Users\johnsmith\.searchsoft\ats-user.properties`. This .properties file must have an entry for `user_code`. An example `ats-user.properties` file follows. It does not matter if you use colons or equals signs to separate keys and values. Spaces immediately before and after these colons and/or equals signs do not matter.

        user_code=A

    The `user_code` value must be exactly one letter. Consult with Tara Hasselschwert if you do not know which letter to use. **If this file exists, the values in it are probably already fine, and you probably do not need to make any changes. This setup guide is for both employees migrating from the old ATE implementation environment, and employees who have never used the old environment. So if you are a longtime ATE team member, you should not need to worry about this step.**

2.  Uninstall all versions of SQL Server from your computer (see https://docs.microsoft.com/en-us/sql/sql-server/install/uninstall-an-existing-instance-of-sql-server-setup?view=sql-server-ver15&tabs=Windows10).
3.  Make the Kansas City network accessible by connecting to Cato Client.
4.  ⚠️⚠️⚠️**The automated setup script the next steps run will delete any database named `nonproductionapplicanttrackingenterprise` on your computer's SQL Server instance. Make sure you either do not have a database with that name, or do not care if it is deleted. The setup script will also delete the directory `C:\applicant-tracking-enterprise-implementation` (assuming your main drive is `C`). This directory may already exist if you have already run the setup script. Make sure you do not need anything in this directory before proceeding. Additionally, the setup script will restart your computer if Hyper-V is not already enabled on your computer. Make sure you do not have any unsaved work that a restart would cause you to lose before running the setup script.**
5.  Open the "Run" window by pressing Windows key + R (or by opening the Start Menu, typing "run", and pressing the enter key).
6.  Paste in the following text and then press the enter key:

        powershell -Command "Start-Process PowerShell -Verb RunAs"

7.  A PowerShell window will appear. Paste in the following text and then press the enter key:

        Set-ExecutionPolicy Bypass -Scope Process -Force;Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/peopleadmin/applicant-tracking-enterprise-implementation-bootstrap/raw/master/applicant-tracking-enterprise-implementation-bootstrap.ps1')

8.  If Hyper-V is not enabled on your computer, you computer will restart shortly after the script starts running. If this happens, resume setup by running the setup script again (in other words, go back to step #5).
9.  You will be prompted for your Bitbucket username and password (see prerequisites section). Type in your username and press enter. Paste in or type in your password and press enter. You should not need to enter these credentials again (until/unless you switch computers), but please store them somewhere anyway.
10. Next the script will pause in order to give you time to setup Docker. Do not close the PowerShell window the setup script is running in. There should be a new icon on your desktop titled "Docker Desktop". Double click it. (See https://token2shell.com/howto/docker/sharing-windows-folders-with-containers/ for screenshots illustrating the following instructions.) Once Docker Desktop starts, right click the Docker Desktop icon in your system tray (the icon looks like a whale). You may have to first click an up arrow in the system tray to get to the Docker Desktop icon (Windows usually only shows a certain number of icons in the system tray, hiding the rest in this up arrow menu). Click "Settings..." and then "Shared Drives". Check the box next to your main drive's letter (usually `C`). Click "Apply". Enter your Windows username and password when prompted. If the checkbox becomes unchecked after enter your credentials, you will have to create a local user account and use those credentials instead (see https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/). After making this change in your Docker settings, go back to the PowerShell window where the setup script is running, and press the enter key. The script will then continue running.

After this script finishes running, you will have everything you need to run ATE locally and commit SQL to common-log. Open `C:\applicant-tracking-enterprise-implementation` (assuming your main drive is `C`) to view the files that have been downloaded. New `my-log`, `common-log`, etc. directories will be in that directory. You do not need to continue to use PowerShell. You can execute `ant run`, etc. in either Command Prompt or PowerShell.

There will be a file named `applicant-tracking-enterprise-implementation.bat` on your desktop. If you double click this file, a Command Prompt window ready to receive `ant` commands will open. (This is basically a shortcut that makes it easier to get to the implementation environment.)

If something goes wrong while the setup script is running, or you are unable to run the application, try running the setup script again. It may take a while to run, but it is designed to be re-runnable, and running it again may resolve the issue(s) you are experiencing.
